#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side set_side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    our_side = set_side;

    if (set_side == WHITE)
    {
        their_side = BLACK;
    } else {
        their_side = WHITE;
    }




    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}


void Player::SetBoard(char data[])
{
    board.setBoard(data);
}

/* GetAllMoves
 *
 *  Returns a vector (of Melements) of all the possible moves on this board
 *  for a given side. The Melements each contain the move that is possible
 *  and the updated board with the move made.
 *
 */
std::vector<Melement*> Player::GetAllMoves(Board *board, Side side)
{
    //TODO: Implement the function that checks for all the valid moves on
    //this board and returns the vector.

    Move *temp_move;
    Board *temp_board;
    std::vector<Melement*> moves;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            temp_move = new Move(i, j);

            if (board->checkMove(temp_move, side))
            {
                temp_board = board->copy();
                temp_board->doMove(temp_move, side);
                moves.push_back(new Melement(temp_move, temp_board));
            }
        }
    }


    return moves;
}



/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    Move *make_move;
    board.doMove(opponentsMove, their_side);

    std::vector<Melement*> all_moves;

    all_moves = GetAllMoves(&board, our_side);

    if (all_moves.size() == 0)
    {
        return NULL;
    }

    make_move = MinMax(all_moves);

    // // corners
    // if (checkCorner(board, our_side) != NULL)
    // {
    //     make_move = checkCorner(board, our_side);
    // }

    board.doMove(make_move, our_side);

    return make_move;
}


Move *Player::MinMax(std::vector<Melement*> candidates)
{
    //TODO: Implement the min-max optimization algorithm by calling heuristics
    //on each of the vector elements
    std::vector<Melement*> level2;
    int max_score = -123924;
    int max_index = 0;

    for (unsigned int i = 0; i < candidates.size(); i++)
    {
        int min_score = 123924;
        level2 = GetAllMoves(candidates[i]->eboard, their_side);

        for (unsigned int j = 0; j < level2.size(); j++)
        {
            level2[j]->score = CalScore(GetAllMoves(level2[j]->eboard, our_side));

            // cerr << "(" << level2[j]->emove->x << ", ";
            // cerr << level2[j]->emove->y << ")" << " : " << level2[j]->score << endl;

            if (level2[j]->score < min_score)
            {
                min_score = level2[j]->score;
            }
        }

//        cerr << "Board " << i << " has score " << min_score << endl;
        candidates[i]->score = min_score;

        if (candidates[i]->score > max_score)
        {
            max_score = candidates[i]->score;
            max_index = i;
        }
    }

    Move *ret = new Move(0, 0);
    ret = candidates[max_index]->emove;

//    cerr << "Chose move " << "(" << ret->x << ", " << ret->y << ")" << " Board #" << max_index << " : " << max_score << endl;
    return ret;
}


/*  MinMax
 *
 *  Performs min-max optimization and picks the best move to make given a
 *  vector of possible moves.
 *
 */
int Player::CalScore(std::vector<Melement*> candidates)
{
    //TODO: Implement the min-max optimization algorithm by calling heuristics
    //on each of the vector elements
    std::vector<Melement*> level2;
    int max_score = -123924;

    for (unsigned int i = 0; i < candidates.size(); i++)
    {
        int min_score = 123924;
        level2 = GetAllMoves(candidates[i]->eboard, their_side);

        for (unsigned int j = 0; j < level2.size(); j++)
        {
            level2[j]->score = level2[j]->eboard->Heuristic(our_side);


            // cerr << "(" << level2[j]->emove->x << ", ";
            // cerr << level2[j]->emove->y << ")" << " : " << level2[j]->score << endl;

            if (level2[j]->score < min_score)
            {
                min_score = level2[j]->score;
            }
        }

//        cerr << "Board " << i << " has score " << min_score << endl;
        candidates[i]->score = min_score;

        if (candidates[i]->score > max_score)
        {
            max_score = candidates[i]->score;
        }
    }
    
//    cerr << "Chose move " << "(" << ret->x << ", " << ret->y << ")" << " Board #" << max_index << " : " << max_score << endl;
    return max_score;
}

Move *Player::checkCorner(Board board, Side side)
{
    Move *temp;
    Move *next_move = NULL;
    temp = new Move(0, 0);
    if (board.checkMove(temp, side))
    {
        delete next_move;
        next_move = temp;
    }
    else
    {
        delete temp;
    }

    temp = new Move(0, 7);
    if (board.checkMove(temp, side))
    {
        delete next_move;
        next_move = temp;
    }
    else
    {
        delete temp;
    }

    temp = new Move(7, 0);
    if (board.checkMove(temp, side))
    {
        delete next_move;
        next_move = temp;
    }
    else
    {
        delete temp;
    }

    temp = new Move(7, 7);
    if (board.checkMove(temp, side))
    {
        delete next_move;
        next_move = temp;
    }
    else
    {
        delete temp;
    }

    return next_move;
}
