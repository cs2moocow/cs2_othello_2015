#include "common.h"
#include "board.h"

struct Melement
{
    //Move element stores the move made to generate the associated board
    Move  *emove;
    Board *eboard;
    int   score;

    //Constructor
    //Accepts a move and board argument (score set automatically)
    Melement(Move *emove, Board *eboard)
    {
        this->emove = emove;
        this->eboard = eboard;
        this->score = -64;
    }

    Melement()
    {
        this->emove = NULL;
        this->eboard = NULL;
        this->score = -64;
    }

    //Destructor automatically frees emove and eboard. Copy the contents back
    //before making the struct go out of scope if you want to keep them.
    ~Melement()
    {
        delete emove;
        delete eboard;
    }

};