#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include "melement.h"
#include <vector>


using namespace std;

class Player {
private:
    Board board;
    Side our_side;
    Side their_side;

public:
    Player(Side set_side);
    ~Player();
    
    vector<Melement*> GetAllMoves(Board *board, Side side);

    Move *doMove(Move *opponentsMove, int msLeft);

    Move *MinMax(vector<Melement*> candidates);

    Move *checkCorner(Board board, Side side);

    int CalScore(std::vector<Melement*> candidates);

    void SetBoard(char data[]);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};






#endif
